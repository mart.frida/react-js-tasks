import React from "react";
import { Delete } from "../delete/delete";
import { Edit } from "../edit/edit";

const ListBody = (props) => {
  return props.data.map((el, index) => {
    return (
      <div key={index * 3 + "r"}>
        <Edit></Edit>
        <Delete></Delete>
        <span>{el.title}</span>
      </div>
    );
  });
};

export { ListBody };
