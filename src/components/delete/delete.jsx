import React from "react";

const Delete = () => {
  return (
    <button type="button" className="btn btn-danger">
      Delete
    </button>
  );
};

export { Delete };
