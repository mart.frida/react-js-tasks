import React from "react";

const Edit = () => {
  return (
    <button type="button" className="btn btn-primary">
      Edit
    </button>
  );
};

export { Edit };
