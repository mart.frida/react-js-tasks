import React from "react";
import "./input.css"

const Input = ()=>{
  return (
    <input className="new-task" type="text" placeholder="new to-do"></input>
  )
}

export {Input};