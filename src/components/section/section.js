import React from "react";
import Title from "../title/title";
import "./section.css";
import { Input } from "../input/input";
import { ListBody } from "../list-body/list-body";
import data from "../../data/data";

const Section = () => {
  return (
    <section className="to-do-list">
      <Title></Title>
      <Input> </Input>
      <ListBody data={data}></ListBody>
    </section>
  );
};

export default Section;
